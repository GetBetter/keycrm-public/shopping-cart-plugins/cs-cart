<?php


namespace Tygh\Addons\Keycrm\Client;

use KeyCrm\ApiClient as BaseApiClient;
use Tygh\Addons\Keycrm\Client\Http\Client;
use Tygh\Addons\Keycrm\Response\ApiResponse;

/**
 * The class wrapper for base KeyCrm Client.
 * Replaces base http client.
 *
 * @package Tygh\Addons\Keycrm\Client
 */
class ApiClient extends BaseApiClient
{
    /**
     * @inheritdoc
     */
    public function __construct($url, $api_key, $site = null)
    {
        if ('/' !== $url[strlen($url) - 1]) {
            $url .= '/';
        }

        $url = $url .  self::VERSION;

        $this->client = new Client($url, array('apiKey' => $api_key));
        $this->siteCode = $site;
    }
}
