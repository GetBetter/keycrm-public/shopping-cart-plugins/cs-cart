<?php


namespace Tygh\Addons\Keycrm\Client\Http;


use KeyCrm\Http\Client as BaseClient;
use Tygh\Addons\Keycrm\Response\ApiResponse;

/**
 * The class wrapper for base KeyCrm http Client.
 * Adds the ability to resend a request when errors are associated with rate limit.
 *
 * @package Tygh\Addons\Keycrm
 */
class Client extends BaseClient
{
    const DELAY = 300000;

    const ATTEMPT_COUNT = 3;

    const ERROR_RATE_LIMIT_CODE = 503;

    /**
     * @inheritDoc
     */
    public function makeRequest($path, $method, array $parameters = array())
    {
        $result = parent::makeRequest($path, $method, $parameters);
        $returnResult = \Tygh\Addons\Keycrm\Response\ApiResponse::fromOriginalResponse($result);
//        return $returnResult;
        return $returnResult;
    }
}