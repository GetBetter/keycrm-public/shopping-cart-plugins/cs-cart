<?php


namespace Tygh\Addons\Keycrm\Converters;

use Tygh\Addons\Keycrm\Settings;

/**
 * The class provides methods to convert customer data from/into keyCRM format.
 *
 * @package Tygh\Addons\Keycrm\Converters
 */
class OrderConverter
{
    /**
     * Code for initialization surcharge on the crm side.
     */
    const SURCHARGE_CODE = 'payment_surcharge';

    /**
     * @var Settings KeyCRM settings instance.
     */
    private $settings;

    /**
     * OrderConverter constructor.
     *
     * @param Settings $settings KeyCRM settings instance.
     */
    public function __construct (Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Converts order data to KeyCRM format.
     *
     * @param array $order Order data.
     * @param mixed $customer_id Customer identifier.
     *
     * @return array
     */
    public function convertToCrmOrder (array $order, $site)
    {
        $curs = $this->settings->getMapCurs();
        $convertTo = $curs[$order['storefront_id']];
        $result = array(
            'source_id' => (int)$site,
            'source_uuid' => $order['order_id'],
            'buyer_comment' => $order['notes'],
            'manager_comment' => '',
            'discount_amount' => fn_format_price_by_currency($order['subtotal_discount'], CART_PRIMARY_CURRENCY, $convertTo),
            'shipping_price' => fn_format_price_by_currency((float)$order['shipping_cost'], CART_PRIMARY_CURRENCY, $convertTo),
            'wrap_price' => '', //Стоимость подарочной упаковки
            'taxes' => 0,
            'ordered_at' => date('Y-m-d H:i:s', $order['timestamp']), //2021-12-21 14:44:00
            'status' => $this->settings->getExternalOrderStatus($order['status']),
            'orderType' => $this->settings->getOrderType(),
            'orderMethod' => $this->settings->getOrderMethod(),
        );



        if (isset($order['coupons']) && count($order['coupons'])) {
            foreach ($order['coupons'] as $couponKey => $coupon) {
                $result['promocode'] = $couponKey;
                break; // выполняем цикл только один раз
            }
        }

        if (count($order['taxes'])) {
            foreach ($order['taxes'] as $taxItem) {
                $result['taxes'] += $taxItem['tax_subtotal'];
            }
        }

        //buyer
        $result['buyer'] = [
            'full_name' => $order['firstname'] . ' ' . $order['lastname'],
            'email' => $order['email'],
            'phone' => $order['phone'],
        ];

        //shipping
        if (!empty($order['shipping'])) {
            $shipping = reset($order['shipping']);

            $result['shipping'] = array(
                'delivery_service_id' => (int)$this->settings->getExternalShippingType($shipping['shipping_id']),
            );

            if (!empty($order['s_city'])) {
//                $address_parts[] = $order['s_city'];
                $result['shipping']['shipping_address_city'] = $order['s_city'];
            }

            if (!empty($order['s_country_descr'])) {
                $result['shipping']['shipping_address_country'] = $order['s_country_descr'];
            }

            if (!empty($order['s_state_descr'])) {
                $result['shipping']['shipping_address_region'] = $order['s_state_descr'];
            }

            if (!empty($order['s_zipcode'])) {
                $result['shipping']['shipping_address_zip'] = $order['s_zipcode'];
            }

            if (!empty($order['s_address'])) {
                $result['shipping']['shipping_secondary_line'] = $order['s_address'];
            }

//            if (!empty($order['s_address'])) {
//                $result['shipping']['shipping_receive_point'] = $order['s_address'];
//            }

//            if (!empty($order['s_address_2'])) {
//                $address_parts[] = $order['s_address_2'];
//            }

        }

        // products
        foreach ($order['products'] as $k => $product) {
            $image = fn_get_cart_product_icon($product['product_id'], $order['products'][$k]);

            $price = (float)$product['price'];
            $original_price = (float)$product['original_price'];
            $discount = (float)$product['discount'];

            if ($original_price - $discount > $price) {
                $discount += $original_price - $discount - $price;
            }

            $item = array(
                'sku' => !empty($product['product_code']) ? $product['product_code'] : $product['id'], // артикул товара - заменяется id товара, если поле не заполнено в магазине
                'price' => fn_format_price_by_currency($original_price, CART_PRIMARY_CURRENCY, $convertTo),
                'discount_amount' => fn_format_price_by_currency($discount, CART_PRIMARY_CURRENCY, $convertTo),
                'quantity' => (float)$product['amount'],
                'name' => $product['product'],
                'picture' => $image["detailed"]["image_path"]
            );

            //добавление атрибутов товара
            if (!empty($product['product_options'])) {
                $combination = array();
                $item['properties'] = array();

                foreach ($product['product_options'] as $product_option) {
                    if (empty($product_option['variant_name'])) {
                        continue;
                    }
                    $item['properties'][] = array(
                        'name' => $product_option['option_name'],
                        'value' => $product_option['variant_name']
                    );
                }
            }

            $result['products'][] = $item;
        }

        #todo предоплата по способу оплаты. Нужно ли указывать?
        if (!empty($order['payment_surcharge'])) {
            $item = array(
                'initialPrice' => fn_format_price_by_currency((float)$order['payment_surcharge'], CART_PRIMARY_CURRENCY, $convertTo),
                'quantity' => (float)1,
                'productName' => 'Payment surcharge',
                'product_id' => self::SURCHARGE_CODE,
                'offer' => array(
                    'externalId' => self::SURCHARGE_CODE
                )
            );

            $result['items'][] = $item;
        }

        //payment
        if (
            ! empty($order['payment_id']) &&
            ($paymentId = (int)$this->settings->getExternalPaymentType($order['payment_id']))
        ) {
            $result['payments'][] = [
                'payment_method_id' => $paymentId,
                'amount'            => fn_format_price_by_currency((float)$order['total'], CART_PRIMARY_CURRENCY, $convertTo),
                'status'            => $order['total'] > 0 ? 'not_paid' : 'paid',
            ];
        }
        $result = array_filter($result);
        return $result;
    }

    /**
     * Convert keyCRM order data to store format.
     *
     * @param array $order KeyCRM order data
     *
     * @return array|bool
     */
    public function convertToShopOrder (array $order)
    {
        $company_id = $this->settings->getInternalSite($order['site']);

        $status = $this->settings->getInternalOrderStatus($order['status']);

        if (!$company_id) {
            return false;
        }

        if (!isset($order['discountPercent'])) {
            $order['discountPercent'] = 0;
        }

        if (!isset($order['discount'])) {
            $order['discount'] = 0;
        }

        $result = array(
            'order_id' => !empty($order['externalId']) ? $order['externalId'] : null,
            'firstname' => isset($order['firstName']) ? $order['firstName'] : '',
            's_firstname' => isset($order['firstName']) ? $order['firstName'] : '',
            'b_firstname' => isset($order['firstName']) ? $order['firstName'] : '',
            'lastname' => isset($order['lastName']) ? $order['lastName'] : '',
            's_lastname' => isset($order['lastName']) ? $order['lastName'] : '',
            'b_lastname' => isset($order['lastName']) ? $order['lastName'] : '',
            'company_id' => $company_id,
            'timestamp' => strtotime($order['createdAt']),
            'total' => $order['totalSumm'],
            'subtotal_discount' => $order['summ'] * $order['discountPercent'] / 100 + $order['discount'],
            'discount' => 0,
            'payment_surcharge' => 0,
            'shipping_cost' => isset($order['delivery']['cost']) ? $order['delivery']['cost'] : 0,
            'stored_discount' => 'Y',
            'user_data' => array(
                'firstname' => isset($order['firstName']) ? $order['firstName'] : '',
                'lastname' => isset($order['lastName']) ? $order['lastName'] : '',
                's_firstname' => isset($order['firstName']) ? $order['firstName'] : '',
                's_lastname' => isset($order['lastName']) ? $order['lastName'] : '',
                'b_firstname' => isset($order['firstName']) ? $order['firstName'] : '',
                'b_lastname' => isset($order['lastName']) ? $order['lastName'] : ''
            ),
            'products' => array()
        );

        if (empty($order['customer']['externalId'])) {
            $result['user_id'] = 0;
        } elseif (is_numeric($order['customer']['externalId'])) { // Local user id
            $result['user_id'] = (int)$order['customer']['externalId'];
        }

        if ($status) {
            $result['status'] = $status;
        } elseif (empty($order['externalId'])) {
            $result['status'] = 'O';
        }

        if (!empty($order['email'])) {
            $result['email'] = $result['user_data']['email'] = $result['email'] = $order['email'];
        } elseif (!empty($order['customer']['email'])) {
            $result['email'] = $result['user_data']['email'] = $result['email'] = $order['customer']['email'];
        } else {
            $result['email'] = $result['user_data']['email'] = $result['email'] = $order['id'] . '@example.com';
        }

        if (!empty($order['customerComment'])) {
            $result['notes'] = $order['customerComment'];
        }

        if (!empty($order['managerComment'])) {
            $result['details'] = $order['managerComment'];
        }

        if (!empty($order['delivery'])) {
            $result['shipping_ids'] = isset($order['delivery']['code'])
                ? $this->settings->getInternalShippingType($order['delivery']['code'])
                : '';

            if (!empty($order['delivery']['address'])) {
                $result['s_country'] = $result['b_country'] = $order['delivery']['address']['countryIso'];
                $result['user_data']['s_country'] = $result['user_data']['b_country'] = $order['delivery']['address']['countryIso'];

                if (isset($order['delivery']['address']['region'])) {
                    $result['s_state'] = $result['b_state'] = $order['delivery']['address']['region'];
                    $result['user_data']['s_state'] = $result['user_data']['b_state'] = $order['delivery']['address']['region'];
                }

                if (isset($order['delivery']['address']['city'])) {
                    $result['s_city'] = $result['b_city'] = $order['delivery']['address']['city'];
                    $result['user_data']['s_city'] = $result['user_data']['b_city'] = $order['delivery']['address']['city'];
                }

                if (isset($order['delivery']['address']['text'])) {
                    $result['s_address'] = $result['b_address'] = $order['delivery']['address']['text'];
                    $result['user_data']['s_address'] = $result['user_data']['b_address'] = $order['delivery']['address']['text'];
                }

                if (isset($order['delivery']['address']['index'])) {
                    $result['s_zipcode'] = $result['b_zipcode'] = $order['delivery']['address']['index'];
                    $result['user_data']['s_zipcode'] = $result['user_data']['b_zipcode'] = $order['delivery']['address']['index'];
                }
            }
        }

        if (isset($order['phone'])) {
            $result['user_data']['phone'] = $result['phone'] = $order['phone'];
            $result['user_data']['s_phone'] = $result['user_data']['b_phone'] = $order['phone'];
            $result['s_phone'] = $result['b_phone'] = $order['phone'];
        }

        if (isset($order['paymentType'])) {
            $result['payment_id'] = $this->settings->getInternalPaymentType($order['paymentType']);
        }

        if (!empty($order['items'])) {
            foreach ($order['items'] as $item) {
                $product = array(
                    'base_price' => $item['initialPrice'],
                    'original_price' => $item['initialPrice'],
                    'price' => $item['initialPrice'],
                    'company_id' => $company_id,
                    'product' => $item['offer']['name'],
                    'amount' => $item['quantity'],
                    'original_amount' => $item['quantity'],
                    'product_id' => null,
                    'stored_price' => 'Y',
                    'stored_discount' => 'Y',
                    'is_edp' => 'N',
                    'extra' => array(
                        'is_edp' => 'N',
                    ),
                );

                $product['discount'] = ($item['initialPrice'] * $item['discountPercent'] / 100) + $item['discount'];

                if (!empty($item['offer']['externalId'])) {

                    if ($item['offer']['externalId'] == self::SURCHARGE_CODE) {
                        $result['payment_surcharge'] = $item['initialPrice'];
                        $result['total'] = $result['total'] - $result['payment_surcharge'];

                        continue;
                    }

                    $parts = explode('_', $item['offer']['externalId']);
                    $product['product_id'] = array_shift($parts);

                    if (!empty($parts)) {
                        $product['extra']['product_options'] = array();

                        while ($parts) {
                            $option_id = array_shift($parts);
                            $variant_id = array_shift($parts);

                            if ($option_id && $variant_id) {
                                $product['extra']['product_options'][$option_id] = $variant_id;
                            }
                        }

                        $product['product_options'] = $product['extra']['product_options'];
                    }
                }

                $result['discount'] += $product['discount'];
                $result['products'][] = $product;
            }
        }

        return $result;
    }
}
