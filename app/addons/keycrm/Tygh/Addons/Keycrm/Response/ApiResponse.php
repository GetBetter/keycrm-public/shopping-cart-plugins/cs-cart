<?php


namespace Tygh\Addons\Keycrm\Response;

use KeyCrm\Response\ApiResponse as BaseResponse;

/**
 * The class wrapper for base KeyCrm ApiResponse.
 * Adds the ability to resend the checking a status.
 *
 * @package Tygh\Addons\Keycrm
 */
class ApiResponse extends BaseResponse
{
    /**
     * @inheritDoc
     */
    public function isSuccessful()
    {
        return $this->response['result'] === 'success' ? true : $this->response['success'];
    }

    public static function fromOriginalResponse(BaseResponse $original_response)
    {
        $self = new self($original_response->statusCode);
        $self->response = $original_response->response;

        return $self;
    }
}