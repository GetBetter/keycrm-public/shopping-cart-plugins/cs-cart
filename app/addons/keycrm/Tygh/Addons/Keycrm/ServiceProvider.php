<?php


namespace Tygh\Addons\Keycrm;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Tygh\Addons\Keycrm\Client\ApiClient;
use Tygh\Addons\Keycrm\Converters\CustomerConverter;
use Tygh\Addons\Keycrm\Converters\OrderConverter;
use Tygh\Registry;
use Tygh\Settings as StorageSettings;

/**
 * Class ServiceProvider is intended to register services and components of the "keycrm" add-on to the application
 * container.
 *
 * @package Tygh\Addons\Keycrm
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * @inheritDoc
     */
    public function register(Container $app)
    {
        $app['addons.keycrm.api_client_factory'] = function (Container $app) {
            return function ($host, $api_key) {
                return new ApiClient('https://' . $host, $api_key);
            };
        };

        $app['addons.keycrm.api_client'] = function (Container $app) {
            return call_user_func($app['addons.keycrm.api_client_factory'],
                Registry::get('addons.keycrm.keycrm_host'),
                Registry::get('addons.keycrm.keycrm_api_key')
            );
        };

        $app['addons.keycrm.settings'] = function () {
            return new Settings(StorageSettings::instance());
        };

        $app['addons.keycrm.logger'] = function () {
            $dir = rtrim(Registry::get('config.dir.files'), '/');
            fn_mkdir($dir);

            return new Logger(
                $dir . '/keycrm_logs/keycrm.log',
                DEFAULT_FILE_PERMISSIONS,
                DEFAULT_DIR_PERMISSIONS
            );
        };

        $app['addons.keycrm.converters.customer'] = function (Container $app) {
            return new CustomerConverter($app['addons.keycrm.settings']);
        };

        $app['addons.keycrm.converters.order'] = function (Container $app) {
            return new OrderConverter($app['addons.keycrm.settings']);
        };

        $app['addons.keycrm.service'] = function (Container $app) {
            return new Service(
                $app['addons.keycrm.settings'],
                $app['addons.keycrm.converters.customer'],
                $app['addons.keycrm.converters.order'],
                $app['addons.keycrm.api_client'],
                $app['addons.keycrm.logger']
            );
        };
    }
}
