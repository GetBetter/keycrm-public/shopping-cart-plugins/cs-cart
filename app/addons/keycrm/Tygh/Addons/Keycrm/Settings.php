<?php


namespace Tygh\Addons\Keycrm;

use Tygh\Settings as StorageSettings;

/**
 * The class provides methods for access the add-on settings.
 *
 * @package Tygh\Addons\Keycrm
 */
class Settings
{
    /** @var StorageSettings */
    protected $settings_storage;

    /** @var array Order status to KeyCRM order status */
    protected $map_order_statuses = array();

    /** @var array Order upload to keycrm statuses */
    protected $map_upload_statuses = array();

    /** @var array Payment identifier to KeyCRM payment code */
    protected $map_payment_types = array();

    /** @var array Shipping identifier to KeyCRM shipping code */
    protected $map_shipping_types = array();

    /** @var array Storefront identifier to KeyCRM site code */
    protected $map_sites = array();

    /** @var array Storefront identifier to KeyCRM currency */
    protected $map_curs = array();

    /** @var string KeyCRM order creating method */
    protected $order_method;

    /** @var string KeyCRM order type */
    protected $order_type;

    /**
     * Settings constructor.
     *
     * @param StorageSettings $settings Settings storage instance.
     */
    public function __construct(StorageSettings $settings)
    {
        $this->settings_storage = $settings;

        $this->map_order_statuses = $this->getSettingValue('keycrm_map_order_statuses', true);
        $this->map_payment_types = $this->getSettingValue('keycrm_map_payment_types', true);
        $this->map_shipping_types = $this->getSettingValue('keycrm_map_shipping_types', true);
        $this->map_sites = $this->getSettingValue('keycrm_map_sites', true);
        $this->map_curs = $this->getSettingValue('keycrm_map_curs', true);
        $this->order_method = $this->getSettingValue('keycrm_order_method');
        $this->order_type = $this->getSettingValue('keycrm_order_type');
        $this->map_upload_statuses = $this->getSettingValue('keycrm_map_upload_statuses', true);
    }

    /**
     * Gets KeyCRM order creating method.
     *
     * @return string
     */
    public function getOrderMethod()
    {
        return $this->order_method;
    }

    /**
     * Gets KeyCRM order type.
     *
     * @return string
     */
    public function getOrderType()
    {
        return $this->order_type;
    }

    /**
     * Sets KeyCRM order creating method.
     *
     * @param string $value
     */
    public function setOrderMethod($value)
    {
        $this->order_method = $value;
        $this->setSettingValue('keycrm_order_method', $value);
    }

    /**
     * Sets KeyCRM order type.
     *
     * @param string $value
     */
    public function setOrderType($value)
    {
        $this->order_type = $value;
        $this->setSettingValue('keycrm_order_type', $value);
    }

    /**
     * Gets mapping of order status to KeyCRM order status.
     *
     * @return array
     */
    public function getMapOrderStatuses()
    {
        return $this->map_order_statuses;
    }

    /**
     * Gets statuses for uploading to KeyCRM.
     *
     * @return array
     */
    public function getMapUploadStatuses()
    {
        $map_upload_statusesRes = $this->map_upload_statuses;
        return $map_upload_statusesRes;
    }

    /**
     * Sets mapping of order status to KeyCRM order status.
     *
     * @param array $data
     */
    public function setMapOrderStatuses(array $data)
    {
        $data = array_filter($data);

        $this->map_order_statuses = $data;
        $this->setSettingValue('keycrm_map_order_statuses', $data, true);
    }

    /**
     * Sets statuses for uploading to KeyCRM.
     *
     * @param array $data
     */
    public function setMapUploadStatuses(array $data)
    {
        $data = array_filter($data);

        $this->map_upload_statuses = $data;
        $this->setSettingValue('keycrm_map_upload_statuses', $data, true);
    }



    /**
     * Gets KeyCRM order status by store order status.
     *
     * @param string $internal_value Order status
     *
     * @return bool|string
     */
    public function getExternalOrderStatus($internal_value)
    {
        return $this->findByIndex($this->map_order_statuses, $internal_value);
    }

    /**
     * Gets order status by KeyCRM order status.
     *
     * @param string $external_value KeyCRM order status.
     *
     * @return bool|string
     */
    public function getInternalOrderStatus($external_value)
    {
        return $this->findByValue($this->map_order_statuses, $external_value);
    }

    /**
     * Gets mapping of storefront identifier to KeyCRM site code.
     *
     * @return array
     */
    public function getMapSites()
    {
        return $this->map_sites;
    }

    /**
     * Sets mapping of storefront identifier to KeyCRM site code.
     *
     * @param array $data
     */
    public function setMapSites(array $data)
    {
        $data = array_filter(array_unique($data));

        $this->map_sites = $data;
        $this->setSettingValue('keycrm_map_sites', $data, true);
    }


    /**
     * Sets currency for uploading to KeyCRM.
     *
     * @param array $data
     */
    public function setMapCur(array $data)
    {
        $data = array_filter($data);

        $this->map_curs = $data;
        $this->setSettingValue('keycrm_map_curs', $data, true);
    }

    /**
     * Gets currency for uploading to KeyCRM.
     *
     * @param array $data
     * @return
     */
    public function getMapCurs()
    {
        return $this->map_curs;
    }

    /**
     * Gets KeyCRM site code by storefront identifier.
     *
     * @param int $internal_value Storefront identifier.
     *
     * @return bool|string
     */
    public function getExternalSite($internal_value)
    {
        return $this->findByIndex($this->map_sites, $internal_value);
    }

    /**
     * Gets storefront identifier by KeyCRM site code.
     *
     * @param string $external_value KeyCRM site code.
     *
     * @return bool|int
     */
    public function getInternalSite($external_value)
    {
        return $this->findByValue($this->map_sites, $external_value);
    }

    /**
     * Gets mapping of shipping identifier to KeyCRM shipping code.
     *
     * @return array
     */
    public function getMapShippingTypes()
    {
        return $this->map_shipping_types;
    }

    /**
     * Sets mapping of shipping identifier to KeyCRM shipping code.
     *
     * @param array $data
     */
    public function setMapShippingTypes(array $data)
    {
        $data = array_filter($data);

        $this->map_shipping_types = $data;
        $this->setSettingValue('keycrm_map_shipping_types', $data, true);
    }

    /**
     * Gets KeyCRM shipping code by shipping identifier.
     *
     * @param int $internal_value Shipping identifier.
     *
     * @return bool|string
     */
    public function getExternalShippingType($internal_value)
    {
        return $this->findByIndex($this->map_shipping_types, $internal_value);
    }

    /**
     * Gets shipping identifier by KeyCRM shipping code.
     *
     * @param string $external_value KeyCRM shipping code.
     *
     * @return bool|int
     */
    public function getInternalShippingType($external_value)
    {
        return $this->findByValue($this->map_shipping_types, $external_value);
    }

    /**
     * Gets mapping of payment identifier to KeyCRM payment code.
     *
     * @return array
     */
    public function getMapPaymentTypes()
    {
        return $this->map_payment_types;
    }

    /**
     * Sets mapping of payment identifier to KeyCRM payment code.
     *
     * @param array $data
     */
    public function setMapPaymentTypes(array $data)
    {
        $data = array_filter($data);

        $this->map_payment_types = $data;
        $this->setSettingValue('keycrm_map_payment_types', $data, true);
    }

    /**
     * Gets KeyCRM payment code by payment identifier.
     *
     * @param int $internal_value Payment identifier.
     *
     * @return bool|string
     */
    public function getExternalPaymentType($internal_value)
    {
        return $this->findByIndex($this->map_payment_types, $internal_value);
    }

    /**
     * Gets payment identifier by KeyCRM payment code.
     *
     * @param string $external_value KeyCRM payment code.
     *
     * @return bool|int
     */
    public function getInternalPaymentType($external_value)
    {
        return $this->findByValue($this->map_payment_types, $external_value);
    }

    /**
     * Finds array index by value.
     *
     * @param array $map
     * @param mixed $value
     *
     * @return bool|mixed
     */
    protected function findByValue($map, $value)
    {
        return array_search($value, $map);
    }

    /**
     * Finds value by index from array.
     *
     * @param array         $map
     * @param string|int    $index
     *
     * @return bool|mixed
     */
    protected function findByIndex($map, $index)
    {
        return isset($map[$index]) ? $map[$index] : false;
    }

    /**
     * Sets setting value.
     *
     * @param string        $setting_name
     * @param array|string  $value
     * @param bool          $is_serialized
     */
    protected function setSettingValue($setting_name, $value, $is_serialized = false)
    {
        if ($is_serialized) {
            $value = json_encode($value);
        }

        $this->settings_storage->updateValue($setting_name, $value);
    }

    /**
     * Gets setting value.
     *
     * @param string $setting_name
     * @param bool   $is_serialized
     *
     * @return array|bool|string
     */
    protected function getSettingValue($setting_name, $is_serialized = false)
    {
        $value = $this->settings_storage->getValue($setting_name, 'keycrm');

        if ($is_serialized) {
            $value = @json_decode($value, true);

            if (!is_array($value)) {
                $value = array();
            }
        }

        return $value;
    }
}