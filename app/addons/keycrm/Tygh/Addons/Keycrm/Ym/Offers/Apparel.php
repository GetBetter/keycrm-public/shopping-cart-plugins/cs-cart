<?php


namespace Tygh\Addons\Keycrm\Ym\Offers;

use Tygh\Ym\Offers\Apparel as BaseApparel;

/**
 * Class Apparel
 *
 * @package Tygh\Addons\Keycrm\Ym\Offers
 */
class Apparel extends BaseApparel
{
    /**
     * @inheritdoc
     */
    protected function getApparelOffer($product)
    {
        $this->schema[] = 'purchasePrice';
        if (!in_array('name', $this->schema)) {
            $this->schema[] = 'name';
        }

        $this->offer['attr'] = array_merge($this->offer['attr'], Simple::getKeyCrmOfferAttributes($product));
        $this->offer['items'] = Simple::getKeyCrmOfferItem($this->offer['items'], $product);
    }

    /**
     * @inheritdoc
     */
    protected function buildOfferCombination($product, $combination)
    {
        $result = parent::buildOfferCombination($product, $combination);

        if ($result) {
            $this->offer['attr'] = array_merge($this->offer['attr'], Simple::getKeyCrmOfferAttributes($product, $combination));
            $this->offer['items']['name'] = ApparelSimple::getProductCombinationName($product, $combination);
        }

        return $result;
    }
}
