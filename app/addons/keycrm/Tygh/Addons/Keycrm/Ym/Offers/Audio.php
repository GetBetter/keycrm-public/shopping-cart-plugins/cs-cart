<?php


namespace Tygh\Addons\Keycrm\Ym\Offers;

use Tygh\Ym\Offers\Audio as BaseAudio;

/**
 * Class Audio
 *
 * @package Tygh\Addons\Keycrm\Ym\Offers
 */
class Audio extends BaseAudio
{
    /**
     * @inheritdoc
     */
    public function gatherAdditional($product)
    {
        parent::gatherAdditional($product);

        $this->schema[] = 'purchasePrice';
        if (!in_array('name', $this->schema)) {
            $this->schema[] = 'name';
        }

        $this->offer['attr'] = array_merge($this->offer['attr'], Simple::getKeyCrmOfferAttributes($product));
        $this->offer['items'] = Simple::getKeyCrmOfferItem($this->offer['items'], $product);

        return true;
    }
}
