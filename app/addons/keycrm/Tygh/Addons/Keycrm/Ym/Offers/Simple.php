<?php


namespace Tygh\Addons\Keycrm\Ym\Offers;

use Tygh\Ym\Offers\Simple as BaseSimple;

/**
 * Class Simple
 *
 * @package Tygh\Addons\Keycrm\Ym\Offers
 */
class Simple extends BaseSimple
{
    /**
     * @inheritdoc
     */
    public function gatherAdditional($product)
    {
        parent::gatherAdditional($product);

        $this->schema[] = 'purchasePrice';

        $this->offer['attr'] = array_merge($this->offer['attr'], self::getKeyCrmOfferAttributes($product));
        $this->offer['items'] = $this->getKeyCrmOfferItem($this->offer['items'], $product);

        return true;
    }

    /**
     * Gets offer attributes required for keyCRM.
     *
     * @param array         $product
     * @param array|null    $combination
     *
     * @return array
     */
    public static function getKeyCrmOfferAttributes($product, array $combination = null)
    {
        $offer_id_parts = array(
            $product['product_id']
        );

        if (!empty($combination['combination'])) {
            ksort($combination['combination']);

            foreach ($combination['combination'] as $option_id => $variant_id) {
                $offer_id_parts[] = $option_id;
                $offer_id_parts[] = $variant_id;
            }
        }

        $result = array(
            'id' => implode('_', $offer_id_parts),
            'productId' => $product['product_id'],
            'group_id' => $product['product_id'],
            'quantity' => $combination ? $combination['amount'] : $product['amount']
        );

        return $result;
    }

    /**
     * Get offer items for KeyCRM
     *
     * @param  array $offer_item
     * @param  array $product
     *
     * @return array
     */
    public static function getKeyCrmOfferItem($offer_item, $product)
    {
        if (!empty($product['yml2_purchase_price'])) {
            $offer_item['purchasePrice'] = $product['yml2_purchase_price'];
        }

        if (empty($offer_item['name'])) {
            $offer_item['name'] = $product['product'];
        }

        return $offer_item;
    }
}
