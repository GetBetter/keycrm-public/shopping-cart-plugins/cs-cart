<?php


namespace Tygh\Addons\Keycrm\Ym\Offers;

use Tygh\Ym\Offers\Vendor as BaseVendor;

/**
 * Class Vendor
 *
 * @package Tygh\Addons\Keycrm\Ym\Offers
 */
class Vendor extends BaseVendor
{
    /**
     * @inheritdoc
     */
    public function gatherAdditional($product)
    {
        parent::gatherAdditional($product);

        $this->schema[] = 'purchasePrice';
        if (!in_array('name', $this->schema)) {
            $this->schema[] = 'name';
        }

        $this->offer['attr'] = array_merge($this->offer['attr'], Simple::getKeyCrmOfferAttributes($product));
        $this->offer['items'] = Simple::getKeyCrmOfferItem($this->offer['items'], $product);

        return true;
    }
}
