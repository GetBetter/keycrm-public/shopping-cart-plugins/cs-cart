<?php


/**
 * @var string $mode
 * @var array $auth
 */
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode === 'update' && $_REQUEST['addon'] === 'keycrm') {
    /** @var \Tygh\SmartyEngine\Core $view */
    $view = Tygh::$app['view'];
    /** @var \Tygh\Addons\Keycrm\Settings $settings */
    $settings = Tygh::$app['addons.keycrm.settings'];

    $keycrm_host = Registry::ifGet('addons.keycrm.keycrm_host', '');
    $keycrm_api_key = Registry::ifGet('addons.keycrm.keycrm_api_key', '');

    if (isset($_REQUEST['keycrm_host'])) {
//        $keycrm_host = $_REQUEST['keycrm_host'];
        $keycrm_host = 'https://openapi.keycrm.app';
    }

    if (isset($_REQUEST['keycrm_api_key'])) {
        $keycrm_api_key = $_REQUEST['keycrm_api_key'];
    }


    $keycrm_host = fn_keycrm_filter_keycrm_host($keycrm_host);

    $result = fn_keycrm_validate_keycrm_credentials($keycrm_host, $keycrm_api_key);

    if ($result->isSuccess()) {
        /** @var \KeyCrm\ApiClient $client */
        $client = call_user_func(Tygh::$app['addons.keycrm.api_client_factory'], $keycrm_host,
            $keycrm_api_key);

        $keycrm_order_statuses = array();
        $response = $client->statusesList();
        if ($response->response['result'] === 'success') {
            $newStatusesList = [];
            foreach ($response->response['data'] as $statusesList) {
                $newStatusesList[$statusesList['alias']] = $statusesList;
            }
            $keycrm_order_statuses = $newStatusesList;
        }



        $keycrm_sites = array();
        $response = $client->sitesList();

        if ($response->response['result'] === 'success') {
            $keycrm_sites = $response->response['data'];
        }

        $keycrm_curs = [
            '0' => [
                'id' => 'UAH',
                'name' => 'UAH',
            ],
            '1' => [
                'id' => 'USD',
                'name' => 'USD',
            ],
            '2' => [
                'id' => 'EUR',
                'name' => 'EUR',
            ],
        ];

        $keycrm_payment_types = array();
        $response = $client->paymentTypesList();

        if ($response->isSuccessful()) {
            $keycrm_payment_types = $response->response['data'];
        }

        $keycrm_shipping_types = array();
        $response = $client->deliveryTypesList();

        if ($response->isSuccessful()) {
            $keycrm_shipping_types = $response->response['data'];
        }

        $keycrm_order_methods = array();
        $response = $client->orderMethodsList();

        if ($response->isSuccessful()) {
            $keycrm_order_methods = $response->response['data'];
        }

        $keycrm_order_types = array();
        $response = $client->orderTypesList();

        if ($response->isSuccessful()) {
            $keycrm_order_types = $response->response['data'];
        }


        $order_statuses = fn_get_statuses(STATUSES_ORDER, [], true, true);
        $payment_types = fn_get_payments(array('status' => 'A'));
        $shipping_types = fn_get_shippings(false);
        $checkCurs = $settings->getMapCurs();
        list($storefronts) = fn_get_companies(array(), $auth);
        $view   ->assign('keycrm_curs', $keycrm_curs)
                ->assign('keycrm_connection_status', true)
                ->assign('keycrm_order_statuses', $keycrm_order_statuses)
                ->assign('keycrm_sites', $keycrm_sites)
                ->assign('keycrm_payment_types', $keycrm_payment_types)
                ->assign('keycrm_shipping_types', $keycrm_shipping_types)
                ->assign('keycrm_order_methods', $keycrm_order_methods)
                ->assign('keycrm_order_types', $keycrm_order_types)
                ->assign('order_statuses', $order_statuses)
                ->assign('storefronts', $storefronts)
                ->assign('shipping_types', $shipping_types)
                ->assign('payment_types', $payment_types)
                ->assign('order_method', $settings->getOrderMethod())
                ->assign('order_type', $settings->getOrderType())
                ->assign('map_order_statuses', $settings->getMapOrderStatuses())
                ->assign('map_shipping_types', $settings->getMapShippingTypes())

                ->assign('map_sites', $settings->getMapSites())
                ->assign('map_curs', $checkCurs)
                ->assign('map_payment_types', $settings->getMapPaymentTypes())
                ->assign('map_upload_statuses', $settings->getMapUploadStatuses());


    } else {

        $view->assign('keycrm_connection_status', false);

        if ($action === 'connect') {
            $result->showNotifications();
        }

    }

    $view->assign('keycrm_order_sync_console_cmd', fn_keycrm_get_console_cmd());
}
if ($mode === 'export' && $_REQUEST['addon'] === 'keycrm') {
    /** @var \Tygh\SmartyEngine\Core $view */
    $view = Tygh::$app['view'];
    /** @var \Tygh\Addons\Keycrm\Settings $settings */
    $settings = Tygh::$app['addons.keycrm.settings'];

    if (isset($_REQUEST['export_orderId'])) {
        $export_orderId = $_REQUEST['export_orderId'];
        $exportRes = fn_keycrm_export_order($export_orderId);
        if ($exportRes){
            $view->assign('keycrm_export_status', true);
        }
        else {
            $view->assign('keycrm_export_status', false);
        }
        #todo Добавить уведомление об ошибке
        //$result->showNotifications();
    }
    $a = $_REQUEST;
}