<?php


/**
 * @var string $mode
 */

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] === 'POST'
    && $mode === 'update'
    && $_REQUEST['addon'] === 'keycrm'
) {
    if (isset($_REQUEST['keycrm_mapping'])) {
        /** @var \Tygh\Addons\Keycrm\Settings $settings */
        $settings = Tygh::$app['addons.keycrm.settings'];


        if (isset($_REQUEST['keycrm_mapping']['order_statuses'])) {
            $settings->setMapOrderStatuses((array) $_REQUEST['keycrm_mapping']['order_statuses']);
        }

        if (isset($_REQUEST['keycrm_mapping']['payment_types'])) {
            $settings->setMapPaymentTypes((array) $_REQUEST['keycrm_mapping']['payment_types']);
        }

        if (isset($_REQUEST['keycrm_mapping']['shipping_types'])) {
            $settings->setMapShippingTypes((array) $_REQUEST['keycrm_mapping']['shipping_types']);
        }

        if (isset($_REQUEST['keycrm_mapping']['sites'])) {
            $sites = array_filter((array) $_REQUEST['keycrm_mapping']['sites']);


            $result = fn_keycrm_validate_keycrm_map_sites($sites);
            $result->showNotifications();

            $settings->setMapSites($sites);
        }

        if (isset($_REQUEST['keycrm_mapping']['curs'])) {
            $settings->setMapCur((array) $_REQUEST['keycrm_mapping']['curs']);
        }

        if (isset($_REQUEST['keycrm_settings']['order_type'])) {
            $settings->setOrderType($_REQUEST['keycrm_settings']['order_type']);
        }

        if (isset($_REQUEST['keycrm_settings']['order_method'])) {
            $settings->setOrderMethod($_REQUEST['keycrm_settings']['order_method']);
        }

        if (isset($_REQUEST['keycrm_mapping']['upload_statuses'])) {
            $settings->setMapUploadStatuses($_REQUEST['keycrm_mapping']['upload_statuses']);
        }
        else {
            $settings->setMapUploadStatuses([]);
        }
    }
}