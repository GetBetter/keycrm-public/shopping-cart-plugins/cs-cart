<?php



if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * @var string $mode
 */

if ($mode === 'sync') {
    fn_define('ORDER_MANAGEMENT', true);

    @ignore_user_abort(1);
    @set_time_limit(0);

    /** @var \Tygh\Addons\Keycrm\Service $service */
    $service = Tygh::$app['addons.keycrm.service'];

    $service->syncOrders();

    exit(0);
}