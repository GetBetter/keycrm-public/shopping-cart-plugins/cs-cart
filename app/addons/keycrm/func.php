<?php



use Tygh\Common\OperationResult;
use Tygh\Addons\Keycrm\Service as KeycrmService;
use Tygh\Enum\YesNo;
use Tygh\Registry;

/**
 * Validates keyCRM credentials.
 *
 * @param string $host      KeyCRM host.
 * @param string $api_key   KeyCRM API key.
 *
 * @return OperationResult
 */
function fn_keycrm_validate_keycrm_credentials($host, $api_key)
{
    $result = new OperationResult();

    if (empty($host)) {
        $result->addError('empty_host', __('keycrm.error.host_is_required'));
    }

    if (empty($api_key)) {
        $result->addError('empty_api_key', __('keycrm.error.api_key_is_required'));
    }

    if (!empty($host) && !empty($api_key)) {
        try {
            /** @var \KeyCrm\ApiClient $client */
            $client = call_user_func(Tygh::$app['addons.keycrm.api_client_factory'], $host, $api_key);
            $response = $client->storesList();

            if (!$response->isSuccessful()) {
                $result->addError('client_error', __('keycrm.error.could_not_connect_by_http_code', array(
                    '[code]' => $response->getStatusCode()
                )));
            }
        } catch (Exception $exception) {
            $result->addError('client_error', __('keycrm.error.could_not_connect', array(
                '[reason]' => $exception->getMessage()
            )));
        }
    }

    $result->setSuccess($result->getErrors() === array());

    return $result;
}

/**
 * Validates keyCRM site mapping settings.
 *
 * @param array $value List of site mapping. (company_id => keycrm_shop_id)
 *
 * @return OperationResult
 */
function fn_keycrm_validate_keycrm_map_sites(array $value)
{
    $result = new OperationResult(true);

    if (count($value) !== count(array_unique($value))) {
        $result->setSuccess(false);
        $result->addError('map_sites', __('keycrm.error.map_sites_invalid'));
    }

    return $result;
}

/**
 * Filters the keyCRM host.
 *
 * @param string $host KeyCRM host.
 *
 * @return string
 */
function fn_keycrm_filter_keycrm_host($host)
{
    return trim(preg_replace('/^.*:\/\//', '', $host), "\t\n\0\x0B/");
}

/**
 * Executes instructions after addon install.
 */
function fn_keycrm_install()
{
    fn_set_storage_data(KeycrmService::KEY_SYNC_ORDER_TIME, time());
}

/**
 * Executes instructions after addon uninstall.
 */
function fn_keycrm_uninstall()
{
    fn_set_storage_data(KeycrmService::KEY_SYNC_ORDER_TIME, null);
}

/**
 * Gets console command for sync orders.
 *
 * @return string
 */
function fn_keycrm_get_console_cmd()
{
    $console_cmd = sprintf(
        '*/5 * * * * php %s/%s --dispatch=keycrm.sync --switch_company_id=0',
        DIR_ROOT,
        Registry::get('config.admin_index')
    );

    /**
     * Executed after string of the console command was builded; Allows to modify string of the console command.
     *
     * @param array  $price_list    Data of price list
     * @param string $console_cmd   String of the console command
     */
    fn_set_hook('keycrm_get_console_cmd', $console_cmd);

    return $console_cmd;
}

/**
 * Hook handler for creating order.
 *
 * @param int $order_id Order identifier.
 */
function fn_keycrm_place_order($order_id)
{
    if (AREA !== 'C') {
        return;
    }

    $order = fn_get_order_info($order_id);

    if ($order['is_parent_order'] === 'Y') {
        return;
    }

    /** @var \Tygh\Addons\Keycrm\Service $service */
    $service = Tygh::$app['addons.keycrm.service'];

    $service->createKeyCrmOrder($order);
}

/**
 * Hook handler for changing order status.
 *
 * @param $status_to
 * @param $status_from
 * @param $order_info
 * @param $force_notification
 * @param $order_statuses
 * @param $place_order
 */
function fn_keycrm_change_order_status($status_to, $status_from, $order_info, $force_notification, $order_statuses, $place_order)
{
//    if (AREA !== 'C') {
//        return;
//    }

    /** @var \Tygh\Addons\Keycrm\Service $service */
    $service = Tygh::$app['addons.keycrm.service'];
    $kOrderInfo = $service->isKeyCrmOrderExists($order_info['order_id']);
    if (($status_from == STATUS_INCOMPLETED_ORDER && $status_to != STATUS_INCOMPLETED_ORDER) || !$kOrderInfo ) {
        $order = fn_get_order_info($order_info['order_id']);
        $order['status'] = $status_to;

        if ($order['is_parent_order'] === 'Y') {
            return;
        }

        $createKeyCrmOrderRes = $service->createKeyCrmOrder($order);
    } else {
        if (isset($order_statuses[$status_to]['params']['payment_received']) && $order_statuses[$status_to]['params']['payment_received'] === "Y") {
            $updateKeyCrmOrderStatusRes = $service->updateKeyCrmPayment($order_info, $kOrderInfo['id']);
        }
        $updateKeyCrmOrderStatusRes = $service->updateKeyCrmOrder($order_info, $kOrderInfo['id']);

    }
}


/**
 * Hook for export order.
 *
 * @param $order_id
 * @param $force_notification
 */
function fn_keycrm_export_order($order_id, $force_notification = false)
{
    /** @var \Tygh\Addons\Keycrm\Service $service */
    $service = Tygh::$app['addons.keycrm.service'];
    $posInt = stripos($order_id, '-');
    if ($posInt === false) {
        //echo "Строка '$findme' не найдена в строке '$mystring1'";
        $orderIds[0] = $orderIds[1] = $order_id;
    }
    else {
        $orderIds = explode('-', $order_id);
    }

    for ($orderId = $orderIds[0]; $orderId <= $orderIds[1]; $orderId++) {
        $order = fn_get_order_info($orderId);
        $kOrderInfo = $service->isKeyCrmOrderExists($orderId);
        if (!$kOrderInfo) {
            if (is_null($kOrderInfo['id'])) {
                $createKeyCrmOrderRes = $service->createKeyCrmOrder($order);
                time_nanosleep(0, 500000000);
            }
        }
    }

    return $createKeyCrmOrderRes;
}

/**
 * The "yml_export_get_options_post" hook handler.
 *
 * Actions performed:
 *  - Adds to $options (properties of price list) new option which forbid using external yml categories.
 *
 * @see fn_yml_get_options
 */
function fn_keycrm_yml_export_get_options_post($price_list_id, &$options)
{
    $options['use_yml_categories'] = $options['used_for_keycrm'] === YesNo::NO;
}