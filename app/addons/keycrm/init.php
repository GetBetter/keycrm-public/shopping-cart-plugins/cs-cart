<?php

//https://docs.keycrm.app/
use Tygh\Registry;
use Tygh\Addons\Keycrm\ServiceProvider;

Registry::get('class_loader')->add('KeyCrm', __DIR__ . '/lib');

Tygh::$app->register(new ServiceProvider());

fn_register_hooks(
    'place_order',
    'change_order_status'// ,
    // 'yml_export_get_options_post'
);