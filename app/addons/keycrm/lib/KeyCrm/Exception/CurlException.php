<?php

/**
 * PHP version 5.3
 *
 * Class CurlException
 *
 * @category KeyCrm
 * @package  KeyCrm
 * @author   KeyCrm <integration@keycrm.ru>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://www.keycrm.ru/docs/Developers/ApiVersion4
 */

namespace KeyCrm\Exception;

/**
 * PHP version 5.3
 *
 * Class CurlException
 *
 * @category KeyCrm
 * @package  KeyCrm
 * @author   KeyCrm <integration@keycrm.ru>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://www.keycrm.ru/docs/Developers/ApiVersion4
 */
class CurlException extends \RuntimeException
{
}
