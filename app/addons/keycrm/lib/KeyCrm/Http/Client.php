<?php

/**
 * PHP version 5.3
 *
 * HTTP client
 *
 * @category KeyCrm
 * @package  KeyCrm
 * @author   KeyCrm <integration@keycrm.ru>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://www.keycrm.ru/docs/Developers/ApiVersion4
 */

namespace KeyCrm\Http;

use KeyCrm\Exception\CurlException;
use KeyCrm\Exception\InvalidJsonException;
use KeyCrm\Response\ApiResponse;

/**
 * PHP version 5.3
 *
 * HTTP client
 *
 * @category KeyCrm
 * @package  KeyCrm
 * @author   KeyCrm <integration@keycrm.ru>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://www.keycrm.ru/docs/Developers/ApiVersion4
 */
class Client
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';

    protected $url;
    protected $defaultParameters;

    /**
     * Client constructor.
     *
     * @param string $url               api url
     * @param array  $defaultParameters array of parameters
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($url, array $defaultParameters = array())
    {
        if (false === stripos($url, 'https://')) {
            throw new \InvalidArgumentException(
                'API schema requires HTTPS protocol'
            );
        }

        $this->url = $url;
        $this->defaultParameters = $defaultParameters;
    }

    /**
     * Make HTTP request
     *
     * @param string $path       request url
     * @param string $method     (default: 'GET')
     * @param array|string  $parameters (default: array())
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     *
     * @throws \InvalidArgumentException
     * @throws CurlException
     * @throws InvalidJsonException
     *
     * @return ApiResponse
     */
    public function makeRequest(
        $path,
        $method,
        array $parameters = array()
    ) {
        $allowedMethods = array(self::METHOD_GET, self::METHOD_POST, self::METHOD_PUT);

        if (!in_array($method, $allowedMethods, false)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Method "%s" is not valid. Allowed methods are %s',
                    $method,
                    implode(', ', $allowedMethods)
                )
            );
        }


        $url = $this->url . $path;

        if (self::METHOD_GET === $method && count($parameters)) {
            $parameters = array_merge($this->defaultParameters, $parameters);
            $apiKey = '';
            if (isset($parameters['apiKey'])){
                $apiKey = $parameters['apiKey'];
                unset($parameters['apiKey']);
            }
            $url .= '?' . http_build_query($parameters, '', '&');
        }
        else {
            if (count($parameters) == 1){
                $parameters = $parameters[0];
            }
        }
        if (!isset($apiKey)) {
            $apiKey = $this->defaultParameters['apiKey'];
        }
        $curlHandler = curl_init();
        curl_setopt($curlHandler, CURLOPT_URL, $url);
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandler, CURLOPT_FOLLOWLOCATION, 1);
//        curl_setopt($curlHandler, CURLOPT_FAILONERROR, false);
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, false);
        $headers = array("Authorization: Bearer ". $apiKey, "Content-Type: application/json");
        curl_setopt($curlHandler, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlHandler, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlHandler, CURLOPT_CONNECTTIMEOUT, 30);

        if (self::METHOD_POST === $method || self::METHOD_PUT === $method) {
//            curl_setopt($curlHandler, CURLOPT_POST, true);
            curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, strtoupper($method));
            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $parameters);
        }

        $responseBody = curl_exec($curlHandler);
        $statusCode = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
        $errno = curl_errno($curlHandler);
        $error = curl_error($curlHandler);

        curl_close($curlHandler);

        if ($errno) {
            throw new CurlException($error, $errno);
        }

        return new ApiResponse($statusCode, $responseBody);
    }
}
