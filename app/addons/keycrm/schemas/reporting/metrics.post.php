<?php


use Tygh\Registry;

/** @var array $schema */

$schema['keycrm'] = function () {
    return Registry::get('addons.keycrm.keycrm_host')
        && Registry::get('addons.keycrm.keycrm_api_key')
        && Tygh::$app['addons.keycrm.settings']->getMapSites();
};

return $schema;