<?php



/**
 * @param string $new_value
 * @param string $old_value
 */
function fn_settings_actions_addons_keycrm_keycrm_host(&$new_value, $old_value)
{
    $new_value = fn_keycrm_filter_keycrm_host($new_value);
}