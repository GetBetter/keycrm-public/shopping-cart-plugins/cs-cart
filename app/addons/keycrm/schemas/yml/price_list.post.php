<?php


/**
 * @var array $schema
 */

$schema['default']['general']['used_for_keycrm'] = array(
    'type' => 'checkbox',
    'default' => 'N',
);

return $schema;