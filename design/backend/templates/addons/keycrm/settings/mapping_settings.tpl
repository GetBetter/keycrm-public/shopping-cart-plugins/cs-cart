{script src="js/addons/keycrm/settings.js"}

<div id="keycrm_settings_container">
    {if $keycrm_connection_status}
        {include file="common/subheader.tpl" title=__("keycrm.settings.mapping_cur") target="#collapsable_cur"}
        <div id="collapsable_cur" class="in collapse">
            <div class="control-group setting-wide">
                <strong class="control-label">{__("keycrm.settings.shop_sites")}</strong>
                <div class="controls"><strong class="control-label">{__("keycrm.settings.keycrm_cur")}</strong></div>
            </div>

            {foreach from=$storefronts item="storefront"}
                <div class="control-group setting-wide">
                    <label for="keycrm_mapping_curs_{$storefront.company_id}" class="control-label">{$storefront.company}</label>
                    <div class="controls">
                        <input type="hidden" name="keycrm_mapping[curs][{$storefront.company_id}]">
                        <select id="keycrm_mapping_curs_{$storefront.company_id}" name="keycrm_mapping[curs][{$storefront.company_id}]">
                            <option value="">---</option>
                            {foreach from=$keycrm_curs item="keycrm_cur"}
                                <option value="{$keycrm_cur.id}" {if $map_curs[$storefront.company_id] == $keycrm_cur.id}selected{/if}>{$keycrm_cur.name}</option>
                            {/foreach}

                        </select>
                    </div>
                </div>
            {/foreach}
        </div>
        
        {include file="common/subheader.tpl" title=__("keycrm.settings.mapping_sites") target="#collapsable_sites"}
        <div id="collapsable_sites" class="in collapse">
            <div class="control-group setting-wide">
                <strong class="control-label">{__("keycrm.settings.shop_sites")}</strong>
                <div class="controls"><strong class="control-label">{__("keycrm.settings.keycrm_sites")}</strong></div>
            </div>

            {foreach from=$storefronts item="storefront"}
                <div class="control-group setting-wide">
                    <label for="keycrm_mapping_sites_{$storefront.company_id}" class="control-label">{$storefront.company}</label>
                    <div class="controls">
                        <input type="hidden" name="keycrm_mapping[sites][{$storefront.company_id}]">
                        <select id="keycrm_mapping_sites_{$storefront.company_id}" name="keycrm_mapping[sites][{$storefront.company_id}]">
                            <option value="">---</option>

                            {foreach from=$keycrm_sites item="keycrm_site"}
                                <option value="{$keycrm_site.id}" {if $map_sites[$storefront.company_id] == $keycrm_site.id}selected{/if}>{$keycrm_site.name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            {/foreach}
        </div>

{*        {include file="common/subheader.tpl" title=__("keycrm.settings.mapping_order_statuses") target="#collapsable_order_statuses"}*}
{*        <div id="collapsable_order_statuses" class="in collapse">*}
{*            <div class="control-group setting-wide">*}
{*                <strong class="control-label">{__("keycrm.settings.shop_order_statuses")}</strong>*}
{*                <div class="controls"><strong class="control-label">{__("keycrm.settings.keycrm_order_statuses")}</strong></div>*}
{*            </div>*}

{*            {foreach from=$order_statuses item="order_status"}*}
{*                <div class="control-group setting-wide">*}
{*                    <label for="keycrm_mapping_order_statuses_{$order_status.status}" class="control-label">{$order_status.description}</label>*}
{*                    <div class="controls">*}
{*                        <input type="hidden" name="keycrm_mapping[order_statuses][{$order_status.status}]">*}
{*                        <select id="keycrm_mapping_order_statuses_{$order_status.status}" name="keycrm_mapping[order_statuses][{$order_status.status}]">*}
{*                            <option value="">---</option>*}
{*                            {foreach from=$keycrm_order_statuses item="keycrm_order_status"}*}
{*                                <option value="{$keycrm_order_status.id}" {if $map_order_statuses[$order_status.status] == $keycrm_order_status.id}selected{/if}>{$keycrm_order_status.name}</option>*}
{*                            {/foreach}*}
{*                        </select>*}
{*                    </div>*}
{*                </div>*}
{*            {/foreach}*}
{*        </div>*}

        {include file="common/subheader.tpl" title=__("keycrm.settings.mapping_payment_types") target="#collapsable_payment_types"}
        <div id="collapsable_payment_types" class="in collapse">
            <div class="control-group setting-wide">
                <strong class="control-label">{__("keycrm.settings.shop_payment_types")}</strong>
                <div class="controls"><strong class="control-label">{__("keycrm.settings.keycrm_payment_types")}</strong></div>
            </div>

            {foreach from=$payment_types item="payment_type"}
                <div class="control-group setting-wide">
                    <label for="keycrm_mapping_payment_types_{$payment_type.payment_id}" class="control-label">{$payment_type.payment}</label>
                    <div class="controls">
                        <input type="hidden" name="keycrm_mapping[payment_types][{$payment_type.payment_id}]">
                        <select id="keycrm_mapping_payment_types_{$payment_type.payment_id}" name="keycrm_mapping[payment_types][{$payment_type.payment_id}]">
                            <option value="">---</option>
                            {foreach from=$keycrm_payment_types item="keycrm_payment_type"}
                                <option value="{$keycrm_payment_type.id}" {if $map_payment_types[$payment_type.payment_id] == $keycrm_payment_type.id}selected{/if}>{$keycrm_payment_type.name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            {/foreach}
        </div>


        {include file="common/subheader.tpl" title=__("keycrm.settings.mapping_shipping_types") target="#collapsable_shipping_types"}
        <div id="collapsable_shipping_types" class="in collapse">
            <div class="control-group setting-wide">
                <strong class="control-label">{__("keycrm.settings.shop_shipping_types")}</strong>
                <div class="controls"><strong class="control-label">{__("keycrm.settings.keycrm_shipping_types")}</strong></div>
            </div>

            {foreach from=$shipping_types item="shipping_type"}
                {if $shipping_type.status == 'A'}
                    <div class="control-group setting-wide">
                        <label for="keycrm_mapping_shipping_types_{$shipping_type.shipping_id}" class="control-label">{$shipping_type.shipping}</label>
                        <div class="controls">
                            <input type="hidden" name="keycrm_mapping[shipping_types][{$shipping_type.shipping_id}]">
                            <select id="keycrm_mapping_shipping_types_{$shipping_type.shipping_id}" name="keycrm_mapping[shipping_types][{$shipping_type.shipping_id}]">
                                <option value="">---</option>
                                {foreach from=$keycrm_shipping_types item="keycrm_shipping_type"}
                                    <option value="{$keycrm_shipping_type.id}" {if $map_shipping_types[$shipping_type.shipping_id] == $keycrm_shipping_type.id}selected{/if}>{$keycrm_shipping_type.name}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                {/if}
            {/foreach}
        </div>

{*        {include file="common/subheader.tpl" title=__("keycrm.settings.other") target="#collapsable_other"}*}
{*        <div id="collapsable_other" class="in collapse">*}
{*            <div class="control-group setting-wide">*}
{*                <label class="control-label" for="keycrm_order_method">{__("keycrm.settings.keycrm_order_method")}</label>*}
{*                <div class="controls">*}
{*                    <select id="keycrm_order_method" name="keycrm_settings[order_method]">*}
{*                        <option value="">---</option>*}
{*                        {foreach from=$keycrm_order_methods item="keycrm_order_method"}*}
{*                            <option value="{$keycrm_order_method.id}" {if $order_method == $keycrm_order_method.id}selected{/if}>{$keycrm_order_method.name}</option>*}
{*                        {/foreach}*}
{*                    </select>*}
{*                </div>*}
{*            </div>*}
{*            <div class="control-group setting-wide">*}
{*                <label class="control-label" for="keycrm_order_type">{__("keycrm.settings.keycrm_order_type")}</label>*}
{*                <div class="controls">*}
{*                    <select id="keycrm_order_type" name="keycrm_settings[order_type]">*}
{*                        <option value="">---</option>*}
{*                        {foreach from=$keycrm_order_types item="keycrm_order_type"}*}
{*                            <option value="{$keycrm_order_type.id}" {if $order_type == $keycrm_order_type.id}selected{/if}>{$keycrm_order_type.name}</option>*}
{*                        {/foreach}*}
{*                    </select>*}
{*                </div>*}
{*            </div>*}
{*        </div>*}
{*        <hr>*}

        <!-- ****************************************************** -->
{*        {include file="common/subheader.tpl" title=__("keycrm.settings.mapping_upload_statuses") target="#collapsable_upload_statuses"}*}
        {include file="common/subheader.tpl" title="Передавать заказы в статусах" target="#collapsable_upload_statuses"}
        <div id="collapsable_upload_statuses" class="in collapse">

            {foreach from=$order_statuses item="order_status"}
                <div class="control-group setting-wide">
                    <label for="keycrm_mapping_order_statuses_{$order_status.status}" class="control-label">{$order_status.description}</label>
                    <div class="controls">

                        <input type="checkbox" name="keycrm_mapping[upload_statuses][{$order_status.status}]" {if $map_upload_statuses[$order_status.status] == "on"} checked="checked"{/if}>
{*                        <input type="hidden" name="keycrm_mapping[order_statuses][{$order_status.status}]">*}
{*                        <select id="keycrm_mapping_order_statuses_{$order_status.status}" name="keycrm_mapping[order_statuses][{$order_status.status}]">*}
{*                            <option value="">---</option>*}
{*                            {foreach from=$keycrm_order_statuses item="keycrm_order_status"}*}
{*                                <option value="{$keycrm_order_status.id}" {if $map_order_statuses[$order_status.status] == $keycrm_order_status.id}selected{/if}>{$keycrm_order_status.name}</option>*}
{*                            {/foreach}*}
{*                        </select>*}
                    </div>
                </div>
            {/foreach}
        </div>



    {else}
        <div class="text-error">{__("keycrm.settings.non_connection")}</div>
        <div class="control-group">
            <div class="controls">
                {include file="buttons/button.tpl" but_id="keycrm_settings_connect_link" but_role="action" but_meta="btn-primary" but_href="{"addons.update.connect?addon=keycrm"|fn_url}" but_text=__("keycrm.settings.connect") but_target_id="keycrm_settings_container"}
            </div>
        </div>
    {/if}
<!--keycrm_settings_container--></div>

