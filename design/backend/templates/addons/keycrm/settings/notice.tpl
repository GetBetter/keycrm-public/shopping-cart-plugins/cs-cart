{include file="common/subheader.tpl" title=__("keycrm.settings.instructions.title") target="#collapsable_instructions"}
<div id="collapsable_instructions" class="in collapse">
    <ol>
        <li>{__("keycrm.settings.instructions.step_credentials")}</li>
        <li>{__("keycrm.settings.instructions.step_tab_mapping")}</li>
        <li>{__("keycrm.settings.instructions.step_connection")}</li>
        <li>{__("keycrm.settings.instructions.step_mapping_settings")}</li>
        <!-- <li>{__("keycrm.settings.instructions.step_price_list", ["[href_price_list]" => "yml.manage"|fn_url])}</li> -->
    </ol>
</div>