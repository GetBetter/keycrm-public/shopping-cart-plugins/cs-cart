(function (_, $) {
  $(document).ready(function () {
    $('#keycrm_export_settings_container').on('click', '#keycrm_start_order_export', function (event) {
      event.preventDefault();
      $.ceAjax('request', $(this).attr('href'), {
        method: 'get',
        result_ids: $(this).data('caTargetId'), //keycrm_export_settings_container
        obj: $(this),
        data: {
          export_orderId: $('#keycrm_export_orderId_input').val()
        }
        ,
        callback: function(data){
          console.log(data);
        }
      });
      // console.log($(this).data('caTargetId'));
      return false;
    });
  });
})(Tygh, Tygh.$);