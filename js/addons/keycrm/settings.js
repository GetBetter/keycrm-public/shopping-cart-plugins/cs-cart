(function (_, $) {
  $(document).ready(function () {
    $('#keycrm_settings_container').on('click', '#keycrm_settings_connect_link', function (event) {
      event.preventDefault();
      $.ceAjax('request', $(this).attr('href'), {
        method: 'get',
        result_ids: $(this).data('caTargetId'),
        obj: $(this),
        data: {
          keycrm_host: $('#addon_option_keycrm_keycrm_host').val(),
          keycrm_api_key: $('#addon_option_keycrm_keycrm_api_key').val()
        }
      });
      return false;
    });
  });
})(Tygh, Tygh.$);